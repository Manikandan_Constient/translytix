import { Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Headers, Http, RequestOptions } from '@angular/http';
import { OnInit, ViewEncapsulation } from '@angular/core';
import 'rxjs/Rx';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
declare var swal: any;

@Component({
   selector: 'login-component',
   templateUrl: './app.currentbids.html',
})

export class MyBidsComponent {
  
  ngOnInit() {
      if ($(window).width() <= 560) {
         $("#testDel").css("display","none")
      }else {
         $("#testDel").css("display","block")
      }
   }

   onResize(event) {
      if (event.target.innerWidth <= 560) {
         $("#testDel").css("display","none")
      } else {
         $("#testDel").css("display","block")
      }
   }


  onSwipeLeft(ev) {
     $("#test").css("margin-left","-50px")
     $("#testDel").css("display","block")
  }
  onSwipeRight(ev) {
     $("#test").css("margin-left","0px")
     $("#testDel").css("display","none")
  }
}
