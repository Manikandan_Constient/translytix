import { NgModule, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from '@angular/material';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/app.dashboardcomponent';
import { UsersComponent } from './users/app.usercomponent';
import { DepotComponent } from './depot/app.depotcomponent';
import { MyBidsComponent } from './currentbids/app.mybidscomponent';
import { ShipmentComponent, encodeuri, search } from './shipment/app.shipment';
import { RouterModule } from '@angular/router';
import { DataTableModule } from "angular2-datatable";
import { LayoutModule } from 'ng2-flex-layout';
import { FormsModule } from '@angular/forms';
import { ChartModule } from 'angular2-highcharts';
import { Resource, ResourceParams, ResourceAction, ResourceMethod, ResourceModule } from 'ng2-resource-rest';
import { HttpModule, Http } from '@angular/http';
import { ModalModule } from "ng2-modal";
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { DatepickerModule } from '../node_modules/angular2-material-datepicker/index.js';
import { MaterializeDirective } from "angular2-materialize";
import { Ng2PaginationModule } from 'ng2-pagination'; // <-- import the module

@NgModule({
  imports: [
    BrowserModule,
    MaterialModule.forRoot(),
    RouterModule.forRoot([/*{
      path: '',
      component: DashboardComponent,
      pathMatch: 'full',
    }, {
      path: 'dashboard',
      component: DashboardComponent,
      pathMatch: 'full',
    },*/
      {
        path: '',
        component: ShipmentComponent,
        pathMatch: 'full',
      },
      {
        path: 'shipment',
        component: ShipmentComponent,
        pathMatch: 'full',
      }, {
        path: 'users',
        component: UsersComponent,
        pathMatch: 'full',
      }, {
        path: 'depot',
        component: DepotComponent,
        pathMatch: 'full',
      }, {
        path: 'mybids',
        component: MyBidsComponent,
        pathMatch: 'full',
      }]),
    DatepickerModule,
    DataTableModule,
    LayoutModule,
    ChartModule,
    ResourceModule.forRoot(),
    HttpModule,
    FormsModule,
    ModalModule,
    ToastModule,
    Ng2PaginationModule
  ],
  declarations: [AppComponent, DashboardComponent, MyBidsComponent, ShipmentComponent, UsersComponent, DepotComponent, MaterializeDirective, encodeuri, search],
  bootstrap: [AppComponent]
})
export class AppModule { }
