"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var ng2_cookies_1 = require('ng2-cookies/ng2-cookies');
var ng2_toastr_1 = require('ng2-toastr/ng2-toastr');
require('rxjs/Rx');
var DashboardComponent = (function () {
    function DashboardComponent(_http, toastr) {
        this._http = _http;
        this.toastr = toastr;
        this.options = {
            chart: {
                type: 'pie'
            },
            title: { text: '' },
            plotOptions: {
                pie: {
                    shadow: false,
                    center: ['50%', '50%']
                }
            },
            series: [{
                    name: 'Browsers',
                    data: [["Firefox", 6], ["MSIE", 4], ["Chrome", 7]],
                    size: '60%',
                    innerSize: '20%',
                    showInLegend: true,
                    dataLabels: {
                        enabled: false
                    }
                }]
        };
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.refreshDepots();
        var userCookie = ng2_cookies_1.Cookie.get('translytixuser');
        if (!userCookie) {
            window.location.assign("/login.html");
        }
    };
    DashboardComponent.prototype.refreshDepots = function () {
        var _this = this;
        this._http.get('http://35.162.123.99:8080/gl/shipment')
            .subscribe(function (halls) { return _this.halls = halls.json(); });
    };
    DashboardComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: 'app/dashboard/app.dashboard.html',
        }), 
        __metadata('design:paramtypes', [http_1.Http, ng2_toastr_1.ToastsManager])
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=app.dashboardcomponent.js.map