import { Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Headers, Http, RequestOptions } from '@angular/http';
import { OnInit, ViewEncapsulation } from '@angular/core';
import 'rxjs/Rx';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
declare var swal: any;

@Component({
    selector: 'login-component',
    templateUrl: 'app/laneopportunities/app.laneopportunities.html',
})

export class LaneOpportunitiesComponent {
    public details: LaneOpportunitiesComponent[]
    public laneDetails: LaneOpportunitiesComponent[]
    noRecords = false
    p: number = 1;
    page = this.p;
    count = 0;
    constructor(private _http: Http) { }
    ngOnInit() {
        this.getMyBids(1, '')
    }

    fetchLaneDetails(laneData) {
        this._http.get('http://35.162.123.99:8080/gl/BD_Lane/' + laneData.id)
            .subscribe(laneDetails => {
                console.log(laneDetails.json())
                this.laneDetails = laneDetails.json().data
            });
    }
    getMyBids(ev, param) {
        this.page = ev ? ev : 0
        //param = "' '?"
        param = param == '' ? "' '?" : param + "?"
        this._http.get('http://35.162.123.99:8080/gl/BD_Lane?' + "page=" + this.page + "&limit=10")
            .subscribe(details => {
                console.log(details.json())
                this.details = details.json().data
            });
    }
}
