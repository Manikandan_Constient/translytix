"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/Rx');
var ng2_cookies_1 = require('ng2-cookies/ng2-cookies');
var MyBidsComponent = (function () {
    function MyBidsComponent(_http) {
        this._http = _http;
        this.noRecords = false;
        this.p = 1;
        this.page = this.p;
        this.count = 0;
    }
    MyBidsComponent.prototype.ngOnInit = function () {
        this.getMyBids();
    };
    MyBidsComponent.prototype.getMyBids = function () {
        var _this = this;
        var userCookie = ng2_cookies_1.Cookie.get('translytixuser');
        // userCookie = JSON.stringify(userCookie)
        userCookie = JSON.parse(userCookie);
        var sel_depot = [];
        sel_depot.push(userCookie[0]);
        console.log(sel_depot[0].bidUserId);
        this._http.get('http://35.162.123.99:8080/gl/BD_Bid/' + sel_depot[0].bidUserId)
            .subscribe(function (details) {
            console.log(details.json());
            _this.details = details.json().length > 0 ? details.json() : (_this.noRecords = true);
        });
    };
    MyBidsComponent = __decorate([
        core_1.Component({
            selector: 'login-component',
            templateUrl: 'app/currentbids/app.currentbids.html',
        }), 
        __metadata('design:paramtypes', [http_1.Http])
    ], MyBidsComponent);
    return MyBidsComponent;
}());
exports.MyBidsComponent = MyBidsComponent;
//# sourceMappingURL=app.mybidscomponent.js.map