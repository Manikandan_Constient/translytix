import { Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule, Headers, Http, RequestOptions } from '@angular/http';
import { OnInit, ViewEncapsulation } from '@angular/core';
import 'rxjs/Rx';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
declare var swal: any;

@Component({
  selector: 'login-component',
  templateUrl: 'app/currentbids/app.currentbids.html',
})

export class MyBidsComponent {
  public details: MyBidsComponent[]
  noRecords = false
  p: number = 1;
  page = this.p;
  count = 0;
  constructor(private _http: Http) { }
  ngOnInit() {
    this.getMyBids()
  }

  getMyBids() {
    let userCookie = Cookie.get('translytixuser');
    // userCookie = JSON.stringify(userCookie)
    userCookie = JSON.parse(userCookie)
    let sel_depot = [];
    sel_depot.push(userCookie[0]);
    console.log(sel_depot[0].bidUserId)
    this._http.get('http://35.162.123.99:8080/gl/BD_Bid/' + sel_depot[0].bidUserId)
      .subscribe(details => {
        console.log(details.json())
        this.details = details.json().length > 0 ? details.json() : (this.noRecords = true)
      });
  }
}
