"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/Rx');
var DepotComponent = (function () {
    function DepotComponent(_http) {
        this._http = _http;
    }
    DepotComponent.prototype.ngOnInit = function () {
        this.refreshUsers();
    };
    DepotComponent.prototype.refreshUsers = function () {
        var _this = this;
        this._http.get('http://35.162.123.99:8080/users')
            .subscribe(function (halls) { return _this.halls = halls.json(); });
    };
    DepotComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            templateUrl: 'app/depot/app.depot.html',
        }), 
        __metadata('design:paramtypes', [http_1.Http])
    ], DepotComponent);
    return DepotComponent;
}());
exports.DepotComponent = DepotComponent;
//# sourceMappingURL=app.depotcomponent.js.map